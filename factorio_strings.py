#!/usr/bin/env python3

# Copyright (C) 2021, Simone Colombo <colombo.simone@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import base64
import zlib
import json
import argparse


def decode_string(input_file: str) -> str:
    with open(input_file, "rb") as factorio_data:
        # Skip the first byte
        factorio_data.seek(1)
        factorio_compressed_json = base64.b64decode(factorio_data.read())
        factorio_json = zlib.decompress(factorio_compressed_json).decode("ascii")
        return json.dumps(json.loads(factorio_json), indent=4)


def encode_json(input_file: str) -> str:
    with open(input_file, "r") as factorio_json_file:
        # Parse the input file to ensure correctness of format
        factorio_json = json.dumps(json.load(factorio_json_file))
        factorio_compressed_json = zlib.compress(factorio_json.encode("ascii"))
        factorio_data = base64.b64encode(factorio_compressed_json).decode("ascii")
        # First byte is the version of the format, which is always "0" as of
        # now. Factorio actually reads this and complains if it isn't "0"
        return("0" + factorio_data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Simple factorio string decoder/json encoder"
    )
    parser.add_argument(
        "-d", action="store_true", help="decode string --> json", default=True
    )
    parser.add_argument("-e", action="store_true", help="encode json --> string")
    parser.add_argument("file", help="which file we should read as input", type=str)

    args = parser.parse_args()

    if args.e:
        print(encode_json(args.file))
    else:
        print(decode_string(args.file))
